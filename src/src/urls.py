from django.conf.urls import patterns, include, url
import views
import smartwaiter

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', views.login_view , name='login'),
    url(r'^manage/', include('smartwaiter.urls', namespace='smartwaiter')),
    url(r'^customer/', include('clientside.urls', namespace='clientside')),
    url(r'^admin/', include(admin.site.urls)),
)
