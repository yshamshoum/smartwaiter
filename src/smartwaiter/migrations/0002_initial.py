# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Waiter'
        db.create_table(u'smartwaiter_waiter', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('waiter_id', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('rest', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['smartwaiter.Restaurant'], null=True)),
        ))
        db.send_create_signal(u'smartwaiter', ['Waiter'])

        # Adding model 'Table'
        db.create_table(u'smartwaiter_table', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('num_table', self.gf('django.db.models.fields.IntegerField')(default='0')),
            ('waiter', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['smartwaiter.Waiter'], null=True)),
            ('rest', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['smartwaiter.Restaurant'], null=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=10, null=True)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'smartwaiter', ['Table'])

        # Adding model 'Order'
        db.create_table(u'smartwaiter_order', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('choices', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('table', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['smartwaiter.Table'])),
            ('waiter', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['smartwaiter.Waiter'])),
        ))
        db.send_create_signal(u'smartwaiter', ['Order'])

        # Adding model 'Restaurant'
        db.create_table(u'smartwaiter_restaurant', (
            ('rest_id', self.gf('django.db.models.fields.CharField')(max_length=36, primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
        ))
        db.send_create_signal(u'smartwaiter', ['Restaurant'])


    def backwards(self, orm):
        # Deleting model 'Waiter'
        db.delete_table(u'smartwaiter_waiter')

        # Deleting model 'Table'
        db.delete_table(u'smartwaiter_table')

        # Deleting model 'Order'
        db.delete_table(u'smartwaiter_order')

        # Deleting model 'Restaurant'
        db.delete_table(u'smartwaiter_restaurant')


    models = {
        u'smartwaiter.order': {
            'Meta': {'object_name': 'Order'},
            'choices': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'table': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['smartwaiter.Table']"}),
            'waiter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['smartwaiter.Waiter']"})
        },
        u'smartwaiter.restaurant': {
            'Meta': {'object_name': 'Restaurant'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'rest_id': ('django.db.models.fields.CharField', [], {'max_length': '36', 'primary_key': 'True'})
        },
        u'smartwaiter.table': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'Meta': {'object_name': 'Table'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'num_table': ('django.db.models.fields.IntegerField', [], {'default': "'0'"}),
            'rest': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['smartwaiter.Restaurant']", 'null': 'True'}),
            'waiter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['smartwaiter.Waiter']", 'null': 'True'})
        },
        u'smartwaiter.waiter': {
            'Meta': {'object_name': 'Waiter'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rest': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['smartwaiter.Restaurant']", 'null': 'True'}),
            'waiter_id': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['smartwaiter']