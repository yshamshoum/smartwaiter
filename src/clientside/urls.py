from django.conf.urls import patterns, include, url
import views

urlpatterns = patterns('',
    # Examples:
    url(r'^$', views.home , name='home'),
    url(r'^restaurants/', views.restaurants , name='restaurants'),
    url(r'^choosetable/', views.chooseTable , name='chooseTable'),
    url(r'^booktable/', views.booktable , name='booktable'),
    
)
