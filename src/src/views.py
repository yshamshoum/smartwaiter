from django.contrib.auth import authenticate
from django.contrib.auth.views import login
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from smartwaiter.forms import LoginScr

def login_view(request):
    if request.user.is_authenticated():
        return redirect('/manage/manage')
    if request.method == 'GET':
        form = LoginScr
        context = { 'form' : form}
        return render(request, 'login.html', context )
    else:
        form = LoginScr(request.POST)
        if form.is_valid():
            usern = form.cleaned_data['username']
            paswrd = form.cleaned_data['password']
            user = authenticate(username = usern , password = paswrd)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('/manage/manage/')
            else:
                return HttpResponse('Wrong user or password') # Need to make a page for this error and ask what should we do if the login info doesn't even exist
        else:
            return HttpResponse('please enter username and password correctly') # Error page incase someone didn't put the info the right way need to work on the forms correctly so it can return an error