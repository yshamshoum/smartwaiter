from django.contrib.auth import authenticate, logout
from django.contrib.auth.views import login
from django.http.response import HttpResponse
from django.shortcuts import render, redirect, render_to_response
from smartwaiter.forms import AddWaiter, AddTable
from models import Waiter, Table, Order, Restaurant
import random

def home(request):
    context = {}
    return HttpResponse('home')

def test(request, uuid):
    rest = Restaurant.objects.filter(rest_id__contains = uuid)
    context = {}
    return HttpResponse(rest)

def manage(request):
    if not request.user.is_authenticated():
        return redirect('/manage/login')
    else:
        if request.method == 'GET':
            formw = AddWaiter
            formt = AddTable
            waiters = Waiter.objects.all()
            tables = Table.objects.all() 
            context = { 'formw' : formw , 'waiters' : waiters, 'formt' : formt, 'tables' : tables}
            return render(request, 'manage.html', context)
        else:
            formt = AddTable(request.POST)
            formw = AddWaiter(request.POST)
            if formw.is_valid():
                waiter = Waiter()
                waiter_id = formw.cleaned_data['waiter_id']
                name = formw.cleaned_data['name']
                waiter.waiter_id = waiter_id
                waiter.name = name
                waiter.save()
                return redirect('/manage/manage/')
            else:
                if formt.is_valid():
                    table = Table()
                    num_table = formt.cleaned_data['num_table']
                    table.num_table = num_table
                    table.save()
                    return redirect('/manage/manage/')
                else:
                    return redirect('/manage/manage/')

def deleteWaiter(request, waiter_id):
    waiter = Waiter.objects.get(id = waiter_id)
    for table in Table.objects.filter(waiter = waiter):
        table.waiter = None
        table.save()
    waiter.delete()
    return redirect('/manage/manage/')

def deleteTable(request, num_table):
    table = Table.objects.get(num_table = num_table)
    table.delete()
    return redirect('/manage/manage/')

def deleteOrder(request, order_id):
    order = Order.objects.get(id = order_id)
    order.delete()
    return redirect('/manage/manage/')

def assignTable(request, num_table):
    waiterid = request.GET['waiter_assign']
    table = Table.objects.get(num_table = num_table)
    table.waiter = Waiter.objects.get(id = waiterid)
    table.save()
    return redirect('/manage/manage/')
            

def checkTable(request, num_table, code):
    table = Table.objects.get(num_table = num_table)
    if table.code == code:
        table.active = True
        return HttpResponse('OK')
    else:
        return HttpResponse('wrong')

def openTable(request, num_table):
    table = Table.objects.get(num_table = num_table)
    table.code = random.randrange(999)
    table.active = True
    table.save()
    order = Order()
    order.choices = 'Open - ' + str(table.code)
    order.table = table
    order.waiter = table.waiter
    order.save()
    return HttpResponse('Ok')

def waiters(request):
    if not request.user.is_authenticated():
        return HttpResponse('Please login')
    waiters = Waiter.objects.all()
    orders = []
    for waiter in waiters:
        order = Order.objects.filter(waiter = waiter).values()
        if len(order) != 0:
            orders.append((waiter , order))
    orders2 = Order.objects.all()
    context = {'waiters' : waiters, 'orders' : orders , 'orders2' : orders2}
    return render(request, 'waiters.html' , context)

def order(request, num_table, choice):
    order = Order()
    table = Table.objects.get(num_table = num_table)
    order.table = table
    order.choices = choice
    if order.choices == 'Check':
        table.Active = False
        table.code = ''
        table.save()
        return render(request , 'main.html')
    order.waiter = order.table.waiter
    order.save()
    return HttpResponse(num_table)
    return render(request , 'menu.html' , {'num_table' : num_table})

def customer3(request):
    return render(request, 'table.html')

def booknew(request):
    num_table = request.GET['num_table']
    table = Table.objects.get(num_table = num_table)
    if not table.Active:
        table.code = random.randrange(999)
        table.Active = True
        table.save()
        order = Order()
        order.choices = 'Open - ' + str(table.code)
        order.table = table
        order.waiter = table.waiter
        order.save()
        context = {'number' : num_table}
        return render(request, 'check.html', context)
    else:
        context = {'number' : num_table}
        return render_to_response('check.html', context)


def checknew(request, num_table):
    table = Table.objects.get(num_table = num_table)
    if table.code == request.GET['codenum']:
        table.Active = True
        context = {'num_table' : table.num_table}
        return render(request , 'menu.html' , context)
    else:
        return HttpResponse('wrong')
    
def ordermenu(request, num_table):
    return render(request, 'ordermenu.html' , {'num_table' : num_table})
    
def mainmenu(request, num_table):
    return render(request , 'menu.html' , {'num_table' : num_table})
    
def logout_view(request):
    logout(request)
    return redirect('/')