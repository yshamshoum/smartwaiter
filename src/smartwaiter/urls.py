from django.conf.urls import patterns, include, url
import views

urlpatterns = patterns('',
    # Examples:
    url(r'^$', views.home , name='home'),
    url(r'^manage', views.manage , name='manage'),
    url(r'^(?P<uuid>[a-z0-9]+)', views.test , name='test'),
    url(r'^delete/(?P<waiter_id>\d+)/$', views.deleteWaiter , name='deleteWaiter'),
    url(r'^deletetable/(?P<num_table>\d+)/$', views.deleteTable , name='deleteTable'),
    url(r'^assign/(?P<num_table>\d+)/$', views.assignTable , name='assignTable'),
    url(r'^checktable/(?P<num_table>\d+)/(?P<code>\d+)/$', views.checkTable, name='checkTable'),
    url(r'^booktable/(?P<num_table>\d+)/', views.openTable , name='bookTable'),
    url(r'^waiters/', views.waiters , name='waiters'),
    url(r'^order/(?P<num_table>\d+)/(?P<choice>\w+)/$', views.order , name='order'),
    url(r'^deleteorder/(?P<order_id>\d+)/$', views.deleteOrder , name='deleteOrder'),
    url(r'^restaurant/' , views.customer3 , name = 'customer'),
    url(r'^booknew/' , views.booknew ,  name = 'customer'),
    url(r'^checknew/(?P<num_table>\d+)/' , views.checknew , name = 'customer'),
    url(r'^ordermenu/(?P<num_table>\d+)/' , views.ordermenu , name = 'customer'),
    (r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
    url(r'^mainmenu/(?P<num_table>\d+)/' , views.mainmenu , name = 'customer'),
)
