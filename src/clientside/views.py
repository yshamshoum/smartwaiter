from django.http.response import HttpResponse
from django.shortcuts import render
from smartwaiter.models import Table, Order
import random

def home(request):
    context = {}
    return render(request, 'loading.html', context)

def restaurants(request):
    context = {}
    return render(request, 'restaurants.html', context)

def chooseTable(request):
    context = {}
    return render(request, 'chooseTable.html', context)


def booktable(request):
    num_table = request.GET['num_table']
    table = Table.objects.get(num_table = num_table)
    if not table.Active:
        table.code = random.randrange(999)
        table.Active = True
        table.save()
        order = Order()
        order.choices = 'Open - ' + str(table.code)
        order.table = table
        order.waiter = table.waiter
        order.save()
        context = {'number' : num_table}
        return render(request, 'check.html', context)
    else:
        context = {'number' : num_table}
        return render(request, 'check.html', context)