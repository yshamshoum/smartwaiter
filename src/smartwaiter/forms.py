from django import forms

class LoginScr(forms.Form):
    username = forms.CharField(max_length=100)
    password = forms.CharField(widget = forms.PasswordInput)
    
class AddWaiter(forms.Form):
    waiter_id = forms.CharField(max_length = 10)
    name = forms.CharField(max_length = 100)
    
class AddTable(forms.Form):
    num_table = forms.IntegerField()
    
    