from django.contrib import admin
from django.contrib.auth.models import User
from django.db import models
from django_extensions.db.fields import UUIDField

class Waiter(models.Model):
    waiter_id = models.CharField(max_length = 100)
    name = models.CharField(max_length = 100)
    rest = models.ForeignKey('Restaurant', null = True)
    
    def __unicode__(self):
        return self.name
    
class Table(models.Model):
    id = models.AutoField(primary_key=True)
    num_table = models.IntegerField(default='0')
    waiter = models.ForeignKey('Waiter', null=True)
    rest = models.ForeignKey('Restaurant', null=True)
    code = models.CharField(max_length = 10, null=True)
    Active = models.BooleanField()
    
    def __unicode__(self):
        return str(self.num_table)

class Order(models.Model):
    choices = models.CharField(max_length=10)
    table = models.ForeignKey('Table')
    waiter = models.ForeignKey('Waiter')
        
    def __unicode__(self):
        return self.choices
    
class Restaurant(models.Model):
    rest_id = UUIDField(primary_key=True)
    name = models.CharField(max_length = 100)
    phone = models.CharField(max_length = 100, null = True)
    address = models.CharField(max_length = 100, null = True)
    rest_user = models.ForeignKey(User)
    
    def __unicode__(self):
        return self.name
    
    
admin.site.register(Waiter)
admin.site.register(Table)
admin.site.register(Order)
admin.site.register(Restaurant)